--[[
A generic button class

This code is MIT licensed, see http://www.opensource.org/licenses/mit-license.php
(C) 2010 - 2011 Gideros Mobile 
]]

Button = gideros.class(Sprite)

function Button:init(upState, downState)
	print("*********")
	self.upState = upState
	self.downState = downState
	self.specialState = nil
	self.enabled = false
	--[[
		Types:
		0: normal, 
		1: blue
		2: red
		3: arrow to bubble
		4: x2
		5: x3
		6: ÷2
		7: ÷3
		8: arrow fromx bubble
		9: bom
		10: bom not complete
	--]]
	self.type = 0
	self.common_value = 0 -- the meaning of this value depends on the bubble type
	-------------
	self.arrow_from = nil -- bubble that point the arrow to this bubble
	self.arrow_to = nil -- bubble that is pointed the arrow from this bubble
	self.index = 0 -- the index of button in the bubble list
	self.x_index = 0
	self.y_index = 0
	self.counter = 0 -- check number of time the button is clicked
	
	self.effect = false -- to render sound or glow effect on button after click
	self.press_duration = 0 -- user must hold for x seconds if this value is x
		
	self.focus = false
	self.touch = false
	
	self:updateVisualState(false)

	--[[
	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
	self:addEventListener(Event.MOUSE_MOVE, self.onMouseMove, self)
	self:addEventListener(Event.MOUSE_UP, self.onMouseUp, self)
	--]]

	self:addEventListener(Event.TOUCHES_BEGIN, self.onTouchesBegin, self)
	self:addEventListener(Event.TOUCHES_MOVE, self.onTouchesMove, self)
	self:addEventListener(Event.TOUCHES_END, self.onTouchesEnd, self)
	self:addEventListener(Event.TOUCHES_CANCEL, self.onTouchesCancel, self)
end

function Button:onMouseDown(event)
	if self:hitTestPoint(event.x, event.y) then
		self.focus = true
		self:updateVisualState(true)
		event:stopPropagation()
		if self.enabled == true then
			local gametune = Sound.new("audio/button_down.wav")
			--gametune:play()
		end
		
		-- progression bar
		if self.type == 9 then
			if self.bar == nil then
				local bar_width = self:getWidth()
				local bar_height = self:getHeight()
				local buffer_time = 5
				local bar = Progressbar.new({
					width = bar_width,
					height = bar_height,
					minValue = 0,
					maxValue = 100,
					value = buffer_time, 
					radius = 0,
					textFont = nil,
					textColor = 0xffffff,
					showValue = false,
					textBefore = "Time: ",
					textAfter = "",
					animIncrement = 1,
					animInterval = 10
				})
				bar:setPosition(0, 0)
				self.bar = bar
				
				self:addChild(bar)
				self.enabled = true
				
				bar:animateValue(100, self.common_value + self.common_value * buffer_time / 100)
				
				-- self.common_value = 0 -- reset the common_value to indicate that the button has been pressed
				
				bar:addEventListener("complete", bar_complete, self)
			end
		end
	end
end

-- Bar complete event
function bar_complete(bubble)
	bubble:dispatchEvent(Event.new("time_complete"))
	bubble.bar:removeEventListener("complete", bar_complete, self)
	bubble:removeChild(bubble.bar)
	bubble.bar = nil
	--bubble.onMouseUp(Event.MOUSE_UP)
end
--

function Button:onMouseMove(event)
	if self.focus then
		if not self:hitTestPoint(event.x, event.y) then
			self.focus = false;
			self:updateVisualState(false)
		end
		event:stopPropagation()
	end
end

function Button:onMouseUp(event)
	if self.focus then
		if self.type ~= 0 then
		-- all the effect can implement here when the button is lifted up
			local gametune = Sound.new("audio/button_up.wav")
			gametune:play()
			--self.effect = false
			
			-- progression bar
			if self.type == 9 then
				if self.bar ~= nil then
					self.bar:stop()
					self:removeChild(self.bar)
					self.bar = nil
					
					-- mouse up while the bar has not completed ==> lose
					print "time_not_complete"
					self.type = 0 -- bubble become normal
					self:dispatchEvent(Event.new("time_not_complete"))
				end
			end
		end
		---------------
		
		self.focus = false;
		self:updateVisualState(false)
		self:dispatchEvent(Event.new("click"))
		event:stopPropagation()
		
	end
end

-- if button is on focus, stop propagation of touch events
function Button:onTouchesBegin(event)
	if self:hitTestPoint(event.touch.x, event.touch.y) then
		self.focus = true
		self.touch = true
		self:updateVisualState(true)
		event:stopPropagation()
		if self.enabled == true then
			local gametune = Sound.new("audio/button_down.wav")
			--gametune:play()
		end
		
		-- progression bar
		if self.type == 9 then
			if self.bar == nil then
				local bar_width = self:getWidth()
				local bar_height = self:getHeight()
				print "debug button 9"
				print(bar_width)
				print(bar_height)
				local bar = Progressbar.new({
					width = bar_width,
					height = bar_height,
					minValue = 0,
					maxValue = 105,
					value = 5, 
					radius = 3,
					textFont = nil,
					textColor = 0xffffff,
					showValue = false,
					textBefore = "Time: ",
					textAfter = "",
					animIncrement = 1,
					animInterval = 10,
					border_in = 3,
					border_out = 3,
					border_color = 0xb1b3b3,
					border_opacity = 0.9
				})
				bar:setPosition(0, 0)
				self.bar = bar
				
				self:addChild(bar)
				self.enabled = true
			end
				
			self.bar:animateValue(100, self.common_value)
			
			-- self.common_value = 0 -- reset the common_value to indicate that the button has been pressed
			self.bar:addEventListener("complete", bar_complete, self)
			
			self:dispatchEvent(Event.new("button_touch_down"))
		end
	end
end

-- if button is on focus, stop propagation of touch events
function Button:onTouchesMove(event)
	if self.focus then
		--[[ For multitouch implementation
		local all = event.allTouches
		local is_pressed = false
		for key, value in pairs(all) do
			--print(value.id .. ": " .. value.x .. ", " .. value.y)
			if self:hitTestPoint(value.x, value.y) then
				is_pressed = true
				break
			end
		end
		if not is_pressed then
			self.focus = false;
			self:updateVisualState(false)
		end
		event:stopPropagation()
		--]]
		if not self:hitTestPoint(event.touch.x, event.touch.y) then
			self.focus = false;
			self:updateVisualState(false)
			
			-- for type 9: click and hold, if move out of the button then stop everything
			if self.type == 9 then
				if self.bar ~= nil then
					self.bar:removeEventListener("complete", bar_complete, self)
					self.bar:stop()
					self:removeChild(self.bar)
					self.bar = nil
					
					-- mouse up while the bar has not completed ==> lose
					self.type = 10 -- bubble become normal
					local event = Event.new("time_not_complete")
					event.type = "time_not_complete"
					self:dispatchEvent(event)
					self.touch = false
				end
			end
		end
		event:stopPropagation()
	end
end

-- if button is on focus, stop propagation of touch events
function Button:onTouchesEnd(event)
	if self.touch then
		--[[ For multitouch implementation
		if self.type ~= 0 then
		-- all the effect can implement here when the button is lifted up
			if self:hitTestPoint(event.touch.x, event.touch.y) then
				local gametune = Sound.new("audio/button_up.wav")
				gametune:play()
				--self.effect = false
				
				-- progression bar
				if self.type == 9 then
					if self.bar ~= nil then
						self.bar:removeEventListener("complete", bar_complete, self)
						self.bar:stop()
						self:removeChild(self.bar)
						self.bar = nil
						
						-- mouse up while the bar has not completed ==> lose
						self.type = 0 -- bubble become normal
						local event = Event.new("time_not_complete")
						event.type = "time_not_complete"
						self:dispatchEvent(event)
					end
				end
			end
		end
		---------------
		
		local all = event.allTouches
		local is_pressed = false
		for key, value in pairs(all) do
			if value.id ~= event.touch.id then
				if self:hitTestPoint(value.x, value.y) then
					is_pressed = true
					break
				end
			end
		end
		
		if not is_pressed then
			self.focus = false;
			self:updateVisualState(false)
			
			local event = Event.new("click")
			event.bubble = self
			self:dispatchEvent(event)
			event:stopPropagation()
		end
		--]]
		----------------------------------------------------------------------
		if self.type ~= 0 then
		-- all the effect can implement here when the button is lifted up
			local gametune = Sound.new("audio/button_up.wav")
			gametune:play()
			--self.effect = false
			
			-- progression bar
			if self.type == 9 then
				if self.bar ~= nil then
					self.bar:removeEventListener("complete", bar_complete, self)
					self.bar:stop()
					self:removeChild(self.bar)
					self.bar = nil
					
					-- mouse up while the bar has not completed ==> lose
					self.type = 10 -- bubble become normal
					local event = Event.new("time_not_complete")
					event.type = "time_not_complete"
					self:dispatchEvent(event)
				end
			end
		end
		---------------
		
		self.focus = false;
		self.touch = false;
		self:updateVisualState(false)
		
		local event = Event.new("click")
		event.bubble = self
		self:dispatchEvent(event)
		event:stopPropagation()
	end
end

-- if touches are cancelled, reset the state of the button
function Button:onTouchesCancel(event)
	if self.focus then
		self.focus = false;
		self:updateVisualState(false)
		event:stopPropagation()
	end
end

-- if state is true show downState else show upState
function Button:updateVisualState(state)
	if state then
		if self:contains(self.upState) then
			self:removeChild(self.upState)
		end
		
		if not self:contains(self.downState) then
			self:addChild(self.downState)
		end
	else
		if self:contains(self.downState) then
			self:removeChild(self.downState)
			if self.specialState ~= nil then
				if self:contains(self.specialState) then
					self:removeChild(self.specialState)
					self.specialState = nil
				end
			end
		end
		
		if not self:contains(self.upState) then
			self:addChild(self.upState)
			if self.specialState ~= nil then
				self:addChild(self.specialState)
			end
		end
	end
end
