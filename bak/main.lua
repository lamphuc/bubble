	local cell_duration = 1000
	local current_index = -1
	local speed_timer_duration = 8000
	local red_wait_duration = 800
	local blue_wait_duration = 1000
	local current_turn_index = 0 -- count how many times user has played, and prevent bugs when users click restart multiple times
	local number_state = {}

	local width_no_cell = 2
	local height_no_cell = 2
	local device_width = application:getDeviceWidth()
	local device_height = application:getDeviceHeight()
	local left_magin = device_width / 10;
	local button_gap_factor = 10
	local cell_width = (device_width - 2*left_magin) * button_gap_factor / ((button_gap_factor + 1) * width_no_cell - 1)
	local button_gap = cell_width / button_gap_factor
	local scale = 1

	local score = 0
	local score_increase = 1

	local text = TextField.new(tahomaFont, "")
	text:setPosition(device_width/2, device_height - 100)
	stage:addChild(text)
	
	local score_text = TextField.new(tahomaFont, "Score:")
	score_text:setPosition(device_width/4, device_height - 100)
	stage:addChild(score_text)
	
	local score_real = TextField.new(tahomaFont, "0")
	score_real:setPosition(device_width/4 + 40, device_height - 100)
	stage:addChild(score_real)
	
	function game_start()
		-- Timer to increase speed
		local local_turn_index = current_turn_index
		
		cell_duration = 1000
		blue_wait_duration = 500
		score_increase = 1
		score = 0
		score_real: setText(score)
		local speed_timer = Timer.new(speed_timer_duration, 5)
		speed_timer:addEventListener(Event.TIMER, function(self, e)
			if cell_duration > 500 then
				cell_duration = cell_duration - 50
				score_increase = score_increase + 1
				--print(cell_duration)
			end
		end, speed_timer)
		speed_timer:start()
	
		-- recode
		local bubbles = {}

		for i = 1, height_no_cell do
			for j = 1, width_no_cell do
				local button_default = Bitmap.new(Texture.new("images/button_normal.png"))
				local button_pushed = Bitmap.new(Texture.new("images/button_normal.png"))
				local new_button = Button.new(button_default, button_pushed)
				scale = cell_width/new_button:getWidth()
				if scale < 1 then 
					scale = 1
				end
				
				if button_gap < cell_width/10 then
					button_gap = cell_width/10
				end
				new_button.upState:setScale(scale)
				new_button.downState:setScale(scale)
				new_button.type = 0
				
				new_button:setPosition(left_magin + (j-1) * (cell_width + button_gap), i * (cell_width + button_gap))
				stage:addChild(new_button)
				table.insert(bubbles, new_button)
				new_button.index = #bubbles
				
				---[[
				new_button:addEventListener("click", 
				function()  
					new_button.upState = Bitmap.new(Texture.new("images/button_normal.png"))
					new_button.downState = Bitmap.new(Texture.new("images/button_normal.png"))
					new_button.upState:setScale(scale)
					new_button.downState:setScale(scale)
					new_button:updateVisualState(false)
				end
				)
				--]]
			end
		end

		
		text:setText("Welcome!!!")
		local game_on = true
		local last_index = -1 --last_index is to prevent the next random blue button to be the same as previous one
		
		local function random_bubble()
			if game_on then
				local index  = math.random(1, #bubbles)
				while index == last_index do
					index  = math.random(1, #bubbles)
				end
				current_index = index
				local bubble = bubbles[index]
				local bubble_type = math.random(1, 100)
				local percentage = 95
				if cell_duration < 700 then
					percentage = 90
				end
				
				if bubble_type < percentage then
					bubble.type = 1
					--bubble.previous_type = 0 --normal
					bubble.effect = true
					
					bubble.upState = Bitmap.new(Texture.new("images/button_blue_up.png"))
					bubble.downState = Bitmap.new(Texture.new("images/button_blue_down.png"))
					bubble.specialState = Bitmap.new(Texture.new("numbers/".. math.random(1, 3) ..".png"))
					
					bubble.upState:setScale(scale)
					bubble.downState:setScale(scale)
					bubble.specialState:setScale(scale)
					
					bubble:updateVisualState(false)
					
					local current_counter = bubble.counter
					local blue_timer = Timer.new(cell_duration, 1)
					blue_timer:addEventListener(Event.TIMER, function(self, e)
						if local_turn_index == current_turn_index then
							if (bubble.index == current_index) and (bubble.type == 1) and (bubble.counter == current_counter) then
								bubble.upState = Bitmap.new(Texture.new("images/button_normal.png"))
								bubble.downState = Bitmap.new(Texture.new("images/button_normal.png"))
								bubble.specialState = nil
								
								bubble.upState:setScale(scale)
								bubble.downState:setScale(scale)
								bubble:updateVisualState(false)
								bubble.enabled = false
								bubble.type = 0
								bubble.effect = false
								
								application:vibrate()
								text:setText("You Lose!!!")
								game_on = false
								return
							end
						end
					end, blue_timer)
					blue_timer:start()
				else
					bubble.type = 2
					bubble.effect = true
					bubble.upState = Bitmap.new(Texture.new("images/button_red_up.png"))
					bubble.downState = Bitmap.new(Texture.new("images/button_red_down.png"))
					bubble.upState:setScale(scale)
					bubble.downState:setScale(scale)
					bubble:updateVisualState(false)
					
					local red_timer = Timer.new(red_wait_duration, 1)
					red_timer:addEventListener(Event.TIMER, function(self, e)
						bubble.upState = Bitmap.new(Texture.new("images/button_normal.png"))
						bubble.downState = Bitmap.new(Texture.new("images/button_normal.png"))
						bubble.upState:setScale(scale)
						bubble.downState:setScale(scale)
						bubble:updateVisualState(false)
						bubble.enabled = false
						bubble.type = 0
						bubble.effect = false
						local next_cell_duration = Timer.new(cell_duration, 1)
						next_cell_duration:addEventListener(Event.TIMER, function(self, e)
							random_bubble()
						end, next_cell_duration)
						next_cell_duration:start()
					end, red_timer)
					red_timer:start()
				end

				bubble.enabled = true;
				bubble:addEventListener("click", 
					function()  
						if bubble.type == 2 then
							game_on = false
							application:vibrate()
							text:setText("You Lose!!!")
							print "182"
							return
						end
						
						-- The cell is assign with blue
						if bubble.enabled then
							last_index = index
							bubble.upState = Bitmap.new(Texture.new("images/button_normal.png"))
							bubble.downState = Bitmap.new(Texture.new("images/button_normal.png"))
							bubble.upState:setScale(scale)
							bubble.downState:setScale(scale)
							bubble:updateVisualState(false)
							bubble.type = 0
							bubble.effect = true
							bubble.enabled = false
							bubble.counter = bubble.counter + 1
							game_on = true
							
							score = score + 1
							score_real:setText(score)
							
							if blue_wait_duration < 200 then 
								random_bubble()
							else
								local next_cell_duration = Timer.new(blue_wait_duration, 1)
								next_cell_duration:addEventListener(Event.TIMER, function(self, e)
									blue_wait_duration = blue_wait_duration - 50
									random_bubble()
								end, next_cell_duration)
								next_cell_duration:start()
							end
						end
					end
				)
			end
		end
		random_bubble()
	end

	local button_default = Bitmap.new(Texture.new("images/button_blue_up.png"))
	local button_pushed = Bitmap.new(Texture.new("images/button_blue_down.png"))
	 
	local startButton = Button.new(button_default, button_pushed)
	 
	local x = ((device_width-startButton:getWidth())/2)
	local y = ((device_height-startButton:getHeight())-20)
	 
	startButton:setPosition(x, y)
	stage:addChild(startButton)
	 
	startButton:addEventListener("click", 
		function()  
			current_turn_index = current_turn_index + 1
			game_start()
		end
	)