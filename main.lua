--[[
This code is MIT licensed, see http://www.opensource.org/licenses/mit-license.php
(C) 2010 - 2011 Gideros Mobile 
]]

JSON = (loadfile "classes/json.lua")()

---[[
require "facebook"
facebook:setAppId("694747983905661")
facebook:addEventListener(Event.LOGIN_COMPLETE, function() print("login complete") end)
facebook:addEventListener(Event.LOGIN_ERROR, function() print("login error") end)
facebook:addEventListener(Event.LOGIN_CANCEL, function() print("login cancel") end)
facebook:addEventListener(Event.LOGOUT_COMPLETE, function() print("logout complete") end)
facebook:addEventListener(Event.DIALOG_COMPLETE, function() print("dialog complete") end)
facebook:addEventListener(Event.DIALOG_ERROR, function(event) print("dialog error", event.errorCode, event.errorDescription) end)
facebook:addEventListener(Event.DIALOG_CANCEL, function() print("dialog cancel") end)
-- facebook:addEventListener(Event.REQUEST_COMPLETE, function(event) print("request complete", event.response) end)
-- facebook:addEventListener(Event.REQUEST_ERROR, function(event) print("request error", event.errorCode, event.errorDescription) end)
--]]

sceneManager = SceneManager.new({
	["play"] = Play,
	["setting"] = Setting,
	["menu"] = Menu,
	["end"] = End,
	["test"] = Test,
	["login"] = Login
})

-- constant
-- device_width = application:getDeviceWidth()
-- device_height = application:getDeviceHeight()

device_width = application:getLogicalWidth()
device_height = application:getLogicalHeight()

--[[
-- Event listener
sceneManager:addEventListener("transitionBegin", function() print("manager - transition begin") end)
sceneManager:addEventListener("transitionEnd", function() print("manager - transition end") end)
--]]

stage:addChild(sceneManager)
sceneManager:changeScene("menu")
--sceneManager:changeScene("login")

-- Functions to debug a variable
function table_print (tt, indent, done)
	done = done or {}
	indent = indent or 0
	if type(tt) == "table" then
		local sb = {}
		for key, value in pairs (tt) do
			table.insert(sb, string.rep (" ", indent)) -- indent it
			if type (value) == "table" and not done [value] then
				done [value] = true
				table.insert(sb, "{\n");
				table.insert(sb, table_print (value, indent + 2, done))
				table.insert(sb, string.rep (" ", indent)) -- indent it
				table.insert(sb, "}\n");
			elseif "number" == type(key) then
				table.insert(sb, string.format("\"%s\"\n", tostring(value)))
			else
			table.insert(sb, string.format(
				"%s = \"%s\"\n", tostring (key), tostring(value)))
			end
		end
		return table.concat(sb)
	else
		return tt .. "\n"
	end
end

function to_string( tbl )
	if  "nil"       == type( tbl ) then
		return tostring(nil)
	elseif  "table" == type( tbl ) then
		return table_print(tbl)
	elseif  "string" == type( tbl ) then
		return tbl
	else
		return tostring(tbl)
	end
end