Menu = gideros.class(Sprite)

function Menu:init()
	-- Background
	local background = Bitmap.new(Texture.new("images/menu.jpg"))
	--background_scale = application:getDeviceWidth()/background:getWidth()
	background:setScale(device_width/background:getWidth())
	background:setY((device_height - background:getHeight()) / 2)
	self:addChild(background)

	local button_default = Bitmap.new(Texture.new("images/template.png"))
	local button_pushed = Bitmap.new(Texture.new("images/template.png"))
	 
	local startButton = Button.new(button_default, button_pushed)
	
	if Button == nil then
		print "nil"
	else
		print "not nil"
		print(startButton:getWidth())
	end

	startButton:setScale(0.8, 0.5) -- This one need to change later
	startButton:setPosition(device_width/2 - startButton:getWidth() / 2, device_height/2 + startButton:getHeight() + 5)
	self:addChild(startButton)
	 
	startButton:addEventListener("click", 
		function()  
			sceneManager:changeScene("play", 1, SceneManager.fade, easing.linear)
		end
	)
end

-- Default function
function Menu:onTransitionInBegin()
	print("Setting - enter begin")
end

function Menu:onTransitionInEnd()
	print("Setting - enter end")
end

function Menu:onTransitionOutBegin()
	print("Setting - exit begin")
end

function Menu:onTransitionOutEnd()
	print("Setting - exit end")
end