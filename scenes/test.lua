-- This scene is to test all the new codes like: Facebook connect, database connect
require "ui"

Test = gideros.class(Sprite)

function Test:init(params)
	local function save_data_to_db(event)
		--print("request complete", event.response)
		local data = JSON:decode(event.response)
		--print(data[1].body)
		local user = JSON:decode(data[1].body)
		print(user.id)
		print(user.name)
		print(user.gender)
	end

	facebook:addEventListener(Event.REQUEST_COMPLETE, save_data_to_db)
	facebook:addEventListener(Event.REQUEST_ERROR, function(event) print("request error", event.errorCode, event.errorDescription) end)

	--facebook:graphRequest("me")
	
	
	-- Connect to server using php API
	--[[ 
	local url = "http://192.168.1.187/bubble/submit_score.php";
	local loader = UrlLoader.new(url, UrlLoader.POST, {}, "myusername=username&mypassword=password")
	
	local function onComplete(event)
		print(event.data)
		if event.data == "1"then
			--you have successfully logged in
		else
			--you did not logged in
		end
	end
	 
	local function onError()
		print("error")
	end
	 
	loader:addEventListener(Event.COMPLETE, onComplete)
	loader:addEventListener(Event.ERROR, onError)
	--]]
	
	--[[
	local function networkListener(event)
		if ( event.isError ) then
				print( "Network error!")
		else
				print(event)
				print(event.data)
				print(to_string(event))
		end
	end
	
	local function onError()
		print("error")
	end
	 
	-- postData = "api_key=cc7a88fd393a12c0ea2b5f369685905f354e79a4&game_id=d947ce5863&response=json&username=First Player"
	postData = "api_key=cc7a88fd393a12c0ea2b5f369685905f354e79a4&game_id=d947ce5863&response=json"
	
	-- local url = "https://api.scoreoid.com/v1/createPlayer"
	local url = "https://api.scoreoid.com/v1/getNotification"
	local loader = UrlLoader.new(url, UrlLoader.POST, {}, postData)
	
	loader:addEventListener(Event.COMPLETE, networkListener)
	loader:addEventListener(Event.ERROR, onError)
	--]]
	
	--[[
	local textInputDialog = TextInputDialog.new("my title", "my message", "some text", "Cancel", "OK")

	local function onComplete(event)
		print(event.text, event.buttonIndex, event.buttonText)
	end

	textInputDialog:addEventListener(Event.COMPLETE, onComplete)
	textInputDialog:show()
	--]]
	hideStatusBar(false)
	useScrollView=false
	local textfield2 = TextField2.new("My text2 field")
	textfield2:setPosition(10, 80)
	textfield2:setText("Hello giderans2!")
	textfield2:setSize(150,25)
	if useScrollView==true then
		scrollView:add(textfield2)
	else
		addToRootView(textfield2)
	end

	local textfield4 = TextField2.new("My text2 field")
	textfield4:setPosition(10, 125)
	textfield4:setText("Hello giderans2!")
	textfield4:setSize(150,25)
	if useScrollView==true then
		scrollView:add(textfield4)
	else
		addToRootView(textfield4)
	end	
	
	
	
	local button = Button.new()

	button:setPosition(30, 210)
	button:setSize(130, 30)

	button:setTitle("Random number")
	button:setTitleColor(255,0,0)
	--button:setBGColor(1,0,1)
	button:setFont("Verdana",12)

	if useScrollView==true then
		scrollView:add(button)
	else
		addToRootView(button)
	end
end