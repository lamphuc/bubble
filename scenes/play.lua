Play = gideros.class(Sprite)

function Play:init(params)
	local state = {
		cell_duration = 1000,
		red_wait_duration = 800,
		blue_wait_duration = 1000,
		reducing_weightage = 25,
		current_no_square = 0,
		score_increase = 1,
		score = 0,
		level = 1
	}
	local cell_duration_min = 550
	local red_wait_duration_min = 700
	local blue_wait_duration_min = 550
	local levels = {}
	--levels[1] = 10
	levels[2] = 5
	levels[3] = 10
	levels[4] = 15
	levels[5] = 20
----------------------------------------------------------
	local width_no_cell = 3
	local height_no_cell = 3
	local left_magin = device_width / 10;
	local top_margin = device_height / 4;
	local button_gap_factor = 10
	local cell_width = (device_width - 2*left_magin) * button_gap_factor / ((button_gap_factor + 1) * width_no_cell - 1)
	local button_gap = cell_width / button_gap_factor
	local scale = 1
	local game_on = false -- to control the timing square
	local bubble_bak = nil -- for revive purpose

	--local alba_font = TTFont.new("fonts/ALBA____.TTF", 40)
	local alba_font = Font.new("fonts/ALBA____.txt", "fonts/ALBA____.png")
	
	local text = TextField.new(alba_font, "") -- you win / you lose here
	text:setPosition(device_width/2 - 50, device_height / 10)
	text:setTextColor("0xffffff")
	self:addChild(text)
	
	local score_text = TextField.new(alba_font, "Score:")
	score_text:setPosition(left_magin, device_height / 10)
	score_text:setTextColor("0xffffff")
	self:addChild(score_text)
	
	local score_real = TextField.new(alba_font, "0")
	score_real:setPosition(left_magin + score_text:getWidth() + 10, device_height / 10)
	score_real:setTextColor("0xffffff")
	self:addChild(score_real)
	
	local level_text = TextField.new(alba_font, "Level:")
	level_text:setPosition(device_width - left_magin - level_text:getWidth() - 20, device_height / 10)
	level_text:setTextColor("0xffffff")
	self:addChild(level_text)
	
	local level_real = TextField.new(alba_font, state.level)
	level_real:setPosition(device_width - left_magin - 10, device_height / 10)
	level_real:setTextColor("0xffffff")
	self:addChild(level_real)
	
	local bubbles = {}
	application:setBackgroundColor("0x8E8E88")


	-- Progress bar
	local bar_width = device_width - 2 * left_magin
	local bar_height = 20
	local bar = Progressbar.new({
		width = bar_width,
		height = bar_height,
		minValue = 0,
		maxValue = 100,
		value = 0, 
		radius = 0,
		textFont = nil,
		textColor = 0xffffff,
		showValue = false,
		textBefore = "Time: ",
		textAfter = "",
		animIncrement = 1,
		animInterval = 10
	})
	bar:setPosition(left_magin, top_margin - 2 * bar_height)
	self:addChild(bar)
	--bar:animateValue(100, 2)

	for i = 1, height_no_cell do
		bubbles[i] = {}
		for j = 1, width_no_cell do
			local button_default = Bitmap.new(Texture.new("images/button_normal.png"))
			local button_pushed = Bitmap.new(Texture.new("images/button_normal.png"))
			local new_button = Button.new(button_default, button_pushed)
			scale = cell_width/new_button:getWidth()
			
			if button_gap < cell_width/10 then
				button_gap = cell_width/10
			end
			new_button.upState:setScale(scale)
			new_button.downState:setScale(scale)
			new_button.type = 0
			new_button.x_index = i
			new_button.y_index = j
			
			new_button:setPosition(left_magin + (j-1) * (cell_width + button_gap), top_margin + (i - 1) * (cell_width + button_gap))
			self:addChild(new_button)
			--table.insert(bubbles, new_button)
			bubbles[i][j] = new_button
			--new_button.index = #bubbles
			
			---[[
			new_button:addEventListener("click", 
			function()  
				new_button.upState = Bitmap.new(Texture.new("images/button_normal.png"))
				new_button.downState = Bitmap.new(Texture.new("images/button_normal.png"))
				new_button.upState:setScale(scale)
				new_button.downState:setScale(scale)
				new_button:updateVisualState(false)
			end
			)
			--]]
		end
	end
	
	-- Main timer
	-- function time_function()
	
	-- end
	local timer_duration
	local timer_check
	--	= Timer.new(3, 1)
	--	timer:addEventListener(Event.TIMER, time_function, timer)
	--  timer:start()
		
	function game_start(time, verifier, new_state) --time: to delay the appearance between each button, verifier: boolean type to choose whether to implement the time
		local expected_type = 0
		
		if new_state == nil or new_state.bubble == nil then
			bubble_bak = nil -- for revive purpose
		end
		
		if new_state ~= nil and new_state.next_square_type ~= nil then
			print "for revive purpose for revive purpose for revive purpose for revive purpose for revive purpose for revive purpose "
			expected_type = new_state.next_square_type
			
			print("expected_type here: " .. expected_type)
		end
		
		bar:reset()
		if(verifier ~= true) then
			game_on = true
			bar:reset()
		-- true: start normally after time ==> implement timer here
		-- false: start immediately, means the game is restart or just started
		-- Note: for timer, to remove completely => stop and create a new one
			if timer_duration ~= nil then
				timer_duration:stop()
				-- It's a good practise to remove the listener
				timer_duration:removeEventListener(Event.TIMER, timer_duration_function)
			end
			
			if timer_check ~= nil then
				timer_check:stop() --this is important
				-- It's a good practise to remove the listener
				timer_check:removeEventListener(Event.TIMER, timer_check_function)
			end
			
			if new_state ~= nil and new_state.bubble ~= nil then
				print "new_state here:"
				print("new_state.score: " .. new_state.score)
				print("new_state.level: " .. new_state.level)
				state = new_state
			else
				state = {
					cell_duration = 1000,
					red_wait_duration = 800,
					blue_wait_duration = 1000,
					reducing_weightage = 25,
					current_no_square = 0,
					score_increase = 1,
					score = 0,
					level = 1
				}
				score_real:setText(state.score)
				level_real:setText(state.level)
			end
			
			for i = 1, height_no_cell do
				for j = 1, width_no_cell do
					local revive_check = false
					
					if bubble_bak ~= nil then
						if i == bubble_bak.x_index and j == bubble_bak.y_index then
							revive_check = true
						end
					end
					
					if not revive_check then
						-- reset all the button
						local button_default = Bitmap.new(Texture.new("images/button_normal.png"))
						local button_pushed = Bitmap.new(Texture.new("images/button_normal.png"))
						local new_button = Button.new(button_default, button_pushed)
						
						if button_gap < cell_width/10 then
							button_gap = cell_width/10
						end
						new_button.upState:setScale(scale)
						new_button.downState:setScale(scale)
						new_button.type = 0
						new_button.x_index = i
						new_button.y_index = j
						
						new_button:setPosition(left_magin + (j-1) * (cell_width + button_gap), top_margin + (i - 1) * (cell_width + button_gap))
						self:addChild(new_button)
						--table.insert(bubbles, new_button)
						bubbles[i][j] = new_button
					end
				end
			end
		else
			if state.cell_duration >= (cell_duration_min + state.reducing_weightage) then
				state.cell_duration = state.cell_duration - state.reducing_weightage
			end
			if state.red_wait_duration >= (red_wait_duration_min + state.reducing_weightage) then
				state.red_wait_duration = state.red_wait_duration - state.reducing_weightage
			end
			if state.blue_wait_duration >= (blue_wait_duration_min + state.reducing_weightage) then
				state.blue_wait_duration = state.blue_wait_duration - state.reducing_weightage
			end
		end
		
		print"---================================---"
		print("current_no_square 1: " .. state.current_no_square)
		if (state.current_no_square == 1) then
			return
		end
		
		state.current_no_square = state.current_no_square + 1
		print("current_no_square 3: " .. state.current_no_square)
		
		if not game_on then
			return
		end
		
		function timer_duration_function()
			-- random a position
			local x_index = math.random(1, height_no_cell)
			local y_index = math.random(1, width_no_cell)
			
			-- Revive bubble
			if bubble_bak ~= nil then
				x_index = bubble_bak.x_index
				y_index = bubble_bak.y_index
				print("&&&&& bubble_bak.x_index: " .. bubble_bak.x_index)
				print("&&&&& bubble_bak.y_index: " .. bubble_bak.y_index)
			end
			print("&&&&& bubble x_index: " .. x_index)
			print("&&&&& bubble y_index: " .. y_index)
			-----------------
			
			local bubble = bubbles[x_index][y_index]
			
			-- implement click_success function
			-- add click event: increase score, recursively call game_start()
			
			print("expected_type:expected_type:expected_type:expected_type:expected_type:expected_type: " .. expected_type)
			
			local bubble_type = math.random(1, 100)
			if ((bubble_type < 80 and (expected_type ~= 2 or expected_type ~= 6 or expected_type ~= 7)) or (expected_type == 1 or expected_type == 3 or expected_type == 4 or expected_type == 5 or expected_type == 8 or expected_type == 9 or expected_type == 10)) then
				-- blue
				bubble.upState = Bitmap.new(Texture.new("images/button_blue_up.png"))
				bubble.downState = Bitmap.new(Texture.new("images/button_blue_down.png"))
				bubble.type = 1
				
				-- set up timer and progress bar
				timer_check = Timer.new(state.blue_wait_duration, 1)
				bar:animateValue(100, state.blue_wait_duration / 1000)
				
				-- if expected_type is not blue then might add other attributes
				if expected_type ~= 2 then
					-- adding arrows
					local is_arrow = math.random(1, 100)
					if ((is_arrow < 20 and state.level >= 2 and (expected_type ~= 1 and expected_type ~= 4 and expected_type ~= 5 and expected_type ~= 9 and expected_type ~= 10)) or (expected_type == 3 or expected_type == 8)) then
						print("v2 expected_type:expected_type:expected_type:expected_type:expected_type:expected_type: " .. expected_type)
						local arrow_x = math.random(-1, 1)
						local arrow_y = math.random(-1, 1)
						local new_x_index = arrow_x + bubble.x_index
						local new_y_index = arrow_y + bubble.y_index
						
						if bubble_bak ~= nil then -- For revive purpose
							new_x_index = bubble_bak.arrow_to.x_index
							new_y_index = bubble_bak.arrow_to.y_index
							arrow_x = new_x_index - bubble_bak.x_index
							arrow_y = new_y_index - bubble_bak.y_index
							
							bubble = bubble_bak
						else
							while (new_x_index == 0 or new_x_index > height_no_cell or new_y_index == 0 or new_y_index > width_no_cell or (arrow_x == 0 and arrow_y == 0)) do
								arrow_x = math.random(-1, 1)
								arrow_y = math.random(-1, 1)
								new_x_index = arrow_x + bubble.x_index
								new_y_index = arrow_y + bubble.y_index
							end
						end
						
						local arrow_index = (arrow_x + 1) * 3 + (arrow_y + 2)
						bubble.specialState = Bitmap.new(Texture.new("images/arrows/arrow_".. arrow_index ..".png"))
						bubble.specialState:setScale(scale)
						--bubble.downState = Bitmap.new(Texture.new("images/button_blue_up.png"))
						
						new_bubble = bubbles[new_x_index][new_y_index]
						new_bubble.downState = Bitmap.new(Texture.new("images/button_blue_down.png"))
						new_bubble.downState:setScale(scale)
						new_bubble.enabled = true
						new_bubble.type = 3
						new_bubble.arrow_from = bubble
						
						new_bubble:addEventListener("click", click_success, new_bubble)
						timer_check:addEventListener(Event.TIMER, timer_check_function, new_bubble)
						
						-- The score will increase if the square with arrow is clicked
						bubble.arrow_to = new_bubble
						bubble.type = 8
					else
						local blue_type = math.random(1, 100)
						if (blue_type < 10 and state.level >= 3 and expected_type ~= 1) or expected_type == 4 or expected_type == 5 then
							-- Add x2 and x3 to blue square
							local times_metric = math.random(1, 100)
							if times_metric < 50 or expected_type == 4 then
								bubble.specialState = Bitmap.new(Texture.new("images/attributes/x2.png"))
								bubble.specialState:setScale(scale)
								bubble.type = 4
							elseif times_metric < 50 or expected_type == 5 then
								bubble.specialState = Bitmap.new(Texture.new("images/attributes/x3.png"))
								bubble.specialState:setScale(scale)
								bubble.type = 5
							end
							--------------------------------
						elseif (blue_type < 30 and state.level >= 5) or expected_type == 9 or expected_type == 10 then
							-- Click and hold for .. seconds
							local time_to_hold = math.random(1, 2)
							if bubble_bak ~= nil then
								time_to_hold = bubble_bak.common_value
							end
							bubble.specialState = Bitmap.new(Texture.new("images/numbers/" .. time_to_hold .. ".png"))
							bubble.specialState:setScale(scale)
							bubble.type = 9
							bubble.common_value = time_to_hold
							
							-- event when progression bar is completely
							---[[
							bubble:addEventListener("time_complete", time_complete, bubble)
							--]]
							bubble:addEventListener("time_not_complete", timer_check_function, bubble)
							bubble:addEventListener("button_touch_down", button_touch_down_function, bubble)
							
						end
						
						timer_check:addEventListener(Event.TIMER, timer_check_function, bubble)
					end
				end
				-------------------------------------
				bubble.upState:setScale(scale)
				bubble.downState:setScale(scale)
				bubble:updateVisualState(false)
				bubble.enabled = true
				bubble:addEventListener("click", click_success, bubble)
				
				timer_check:start()
			else
				-- red
				bubble.upState = Bitmap.new(Texture.new("images/button_red_up.png"))
				bubble.downState = Bitmap.new(Texture.new("images/button_red_down.png"))
				bubble.upState:setScale(scale)
				bubble.downState:setScale(scale)
				bubble.enabled = true
				bubble.type = 2
				
				if (state.level >= 4 and expected_type ~= 2) or expected_type == 6 or expected_type == 7 then
					-- Add divide attribute to red square
					local divide_metric = math.random(1, 100)
					if divide_metric < 10 or expected_type == 6 then
						bubble.specialState = Bitmap.new(Texture.new("images/attributes/d2.png"))
						bubble.specialState:setScale(scale)
						bubble.type = 6
					elseif divide_metric < 20 or expected_type == 7 then
						bubble.specialState = Bitmap.new(Texture.new("images/attributes/d3.png"))
						bubble.specialState:setScale(scale)
						bubble.type = 7
					end
				end
				---------------------------------------
				bubble:updateVisualState(false)
				
				bubble:addEventListener("click", timer_check_function, bubble)
				
				-- use timer to check if user not click within the time limit
				timer_check = Timer.new(state.red_wait_duration, 1)
				timer_check:addEventListener(Event.TIMER, click_success, bubble)
				timer_check:start()
				
				-- set up progress bar
				bar:animateValue(100, state.red_wait_duration / 1000)
				-- bar:setPaused(false)
			end
			print ("bubble type check: " .. bubble.type)
		end
		
		function increase_score(score, interval)
			state.score = score + interval
			score_real:setText(state.score)
			
			if state.level < 5 then
				for i = state.level, 5 do
					new_level = state.level + 1
					if score >= levels[new_level] then
						state.level = new_level
						level_real:setText(state.level)
						break
					end
				end
			end
		end
		
		function click_success(bubble, event)
			print("click_success event: ")
			print(event:getType())
			if bubble.type == 8 then
				-- there can be a function to set score
				increase_score(state.score, 1)
				bubble:removeEventListener("click", click_success, bubble)
			else
				-- x2 or x3 only get effect if player click on it
				if bubble.type == 4 then
					state.score_increase = state.score_increase * 2
				elseif bubble.type == 5 then
					state.score_increase = state.score_increase * 3
				end
				
				-- implement effect like sound, score... after player click a square successful
				-- the score can be different for different attributes ==> need to implement it here
				increase_score(state.score, state.score_increase)
				--score = score + score_increase
				
				-- cannot change the order of these 2 lines
				remove_eventListener(bubble)
				
				local next_square = 0
				if state.score == levels[2] then
					next_square = 3
				elseif state.score == levels[3] then
					next_square = math.random(4, 5)
				elseif state.score == levels[4] then
					next_square = math.random(6, 7)
				elseif state.score == levels[5] then
					next_square = 9
				end
				
				print("score: " .. state.score)
				print ("next_square: " .. next_square)
				print "Bubble game_start 1:"
				print(bubble.x_index)
				print(bubble.y_index)
				
				state.next_square_type = next_square
				state.bubble = nil
				
				game_start(cell_duration, true, state)
			end
		end

		function remove_eventListener(bubble) -- This function will reset the button: type => 0, enabled => false, stop timer, remove event listener
			print("current_no_square 2: " .. state.current_no_square)
			print("bubble type: " .. bubble.type)
			if(bubble.type ~= 0) then
				state.current_no_square = state.current_no_square - 1
			end
			print("current_no_square 2v2: " .. state.current_no_square)
			
			bubble.upState = Bitmap.new(Texture.new("images/button_normal.png"))
			bubble.downState = Bitmap.new(Texture.new("images/button_normal.png"))
			bubble.specialState = Bitmap.new(Texture.new("images/template.png"))
			
			bubble.upState:setScale(scale)
			bubble.downState:setScale(scale)
			bubble.specialState:setScale(scale)
			
			bubble:updateVisualState(false)
			bubble.enabled = false
			
			bubble:removeEventListener("click", click_success, bubble)
			bubble:removeEventListener("click", click_success, new_bubble)
			bubble:removeEventListener("click", timer_check_function, bubble)
			
			bubble:removeEventListener("time_not_complete", timer_check_function, bubble)
			
			-- Check for arrow
			if bubble.type == 3 then
				local old_bubble = bubble.arrow_from
				old_bubble.upState = Bitmap.new(Texture.new("images/button_normal.png"))
				old_bubble.downState = Bitmap.new(Texture.new("images/button_normal.png"))
				old_bubble.specialState = Bitmap.new(Texture.new("images/template.png"))
				old_bubble.upState:setScale(scale)
				old_bubble.downState:setScale(scale)
				old_bubble.specialState:setScale(scale)
				old_bubble:updateVisualState(false)
				old_bubble.enabled = false
				old_bubble.type = 0
			end
			
			timer_check:stop()
			timer_check:removeEventListener(Event.TIMER, click_success, bubble)
			timer_check:removeEventListener(Event.TIMER, timer_check_function, bubble)
			timer_check:removeEventListener(Event.TIMER, timer_check_function, new_bubble)
			
			bubble.type = 0
		end
		
		function timer_check_function(bubble, event)
			if bubble.enabled then
				-- reuse the same function as when player click the button: reset and make the button unclickable
				
				-- TODO: optimize this chunks of codes
				local bubble_type = bubble.type
				
				print()
				print "Bubble game_start 2:"
				print(bubble.x_index)
				print(bubble.y_index)
				
				if bubble_type == 6 then
					state.score = math.floor(state.score / 2)
					score_real:setText(state.score)
					remove_eventListener(bubble)
					game_start(cell_duration, true)
				elseif bubble_type == 7 then
					state.score = math.floor(state.score / 3)
					score_real:setText(state.score)
					remove_eventListener(bubble)
					game_start(cell_duration, true)
				elseif bubble_type == 4 or bubble_type == 5 then
					remove_eventListener(bubble)
					game_start(cell_duration, true)
				elseif bubble_type == 9 and bubble.touch == true--[[ and bubble.common_value == 0 --]] then
					timer_check:stop()
					timer_check:removeEventListener(Event.TIMER, timer_check_function, bubble)
				else
					--text:setText("You lose")
					application:vibrate()
					timer_duration:stop()
					timer_check:stop()
					bar:stop()
					if(bubble_type ~= 2 or bubble_type ~= 6 or bubble_type ~= 7) then
						bar:setValue(100)
					end
					game_on = false
					
					-- Back up bubble for revive purpose
					local button_bak = Button.new(bubble.upState, bubble.downState)
					button_bak.specialState = bubble.specialState
					
					if bubble.type == 3 then
						local bubble_to = Button.new(bubble.upState, bubble.downState)
						bubble_to.x_index = bubble.x_index
						bubble_to.y_index = bubble.y_index
						
						bubble = bubble.arrow_from
						button_bak.arrow_to = bubble_to
					end
					
					button_bak.type = bubble.type
					print("***** button_bak.type: " .. button_bak.type)
					print("***** bubble.common_value: " .. bubble.common_value)
					print("***** bubble.x_index: " .. bubble.x_index)
					print("***** bubble.x_index: " .. bubble.x_index)
					button_bak.x_index = bubble.x_index
					button_bak.y_index = bubble.y_index
					button_bak.common_value = bubble.common_value -- for type 9 and 10
					-----------------------------
					
					state.bubble = button_bak
					state.next_square_type = bubble.type
					sceneManager:changeScene("end", 1, SceneManager.fade, easing.linear, 
						{userData = state}
					)
					
					--touch_to_start()
					remove_eventListener(bubble)
					return
				end
				--touch_to_start()
			end
		end
		
		function time_complete(bubble)
			bubble:removeEventListener("time_complete", time_complete, bubble)
		end
		
		function button_touch_down_function(bubble)
			bar:reset()
			--timer_check:removeEventListener(Event.TIMER, timer_check_function, bubble)
		end
		
		if verifier == true then
			timer_duration = Timer.new(state.cell_duration, 1)
		else
			timer_duration = Timer.new(0, 1)
		end
		timer_duration:addEventListener(Event.TIMER, timer_duration_function)
		timer_duration:start()
	end

	-- testing button
	---[[
	local button_default = Bitmap.new(Texture.new("images/button_blue_up.png"))
	local button_pushed = Bitmap.new(Texture.new("images/button_blue_down.png"))
	 
	local startButton = Button.new(button_default, button_pushed)
	 
	startButton:setScale(scale)
	local x = (left_magin)
	local y = ((device_height-startButton:getHeight())-20)
	 
	startButton:setPosition(x, y)
	self:addChild(startButton)
	 
	startButton:addEventListener("click", 
		function()  
			-- bar:stop()
			state.red_wait_duration = 1000
			state.blue_wait_duration = 1000
			state.reducing_weightage = 10
		end
	)
	--]]
	---------------------------
	
	-- Touch screen to continue
	local touch_to_start_label
	if params ~= nil and params.bubble ~= nil then
		touch_to_start_label = TextField.new(Font.new("fonts/ALBAS___.txt", "fonts/ALBAS___.png"), "Touch to continue")
		score_real:setText(params.score)
		level_real:setText(params.level)
		
		-- revive the square that caused the game to finish
		print("Bubble type2: " .. params.bubble.type)
		
		-- Back up bubble
		print "bubble indexes"
		print(params.bubble.x_index)
		print(params.bubble.y_index)
		bubble_bak = bubbles[params.bubble.x_index][params.bubble.y_index]
		bubble_bak.upState = params.bubble.upState
		bubble_bak.downState = params.bubble.downState
		bubble_bak.specialState = params.bubble.specialState
		bubble_bak:updateVisualState(false)
		bubble_bak.arrow_to = params.bubble.arrow_to -- type = 3
		bubble_bak.common_value = params.bubble.common_value -- type = 10
		print("&&&&& bubble_bak.x_index: " .. bubble_bak.x_index)
		print("&&&&& bubble_bak.y_index: " .. bubble_bak.y_index)
		-----------------------------
	else
		touch_to_start_label = TextField.new(Font.new("fonts/ALBAS___.txt", "fonts/ALBAS___.png"), "Touch to start")
	end
	
	touch_to_start_label:setPosition((device_width - touch_to_start_label:getWidth())/2, device_height -touch_to_start_label:getHeight() - 50)
	touch_to_start_label:setTextColor("0xffffff")
	self:addChild(touch_to_start_label)
	
	function touch_to_start()
		local screen_layer = Bitmap.new(Texture.new("images/template.png"))
		screen_layer:setPosition(0,0)
		screen_layer:setScale(device_width / screen_layer:getWidth(), device_height / screen_layer:getHeight())
		self:addChild(screen_layer)
		function screen_touch(event)
			self:removeChild(touch_to_start_label)
			
			-- Check for revive
			local new_state
			if params ~= nil then
				new_state = params
			else
				new_state = nil
				state.level = 6
			end
			
			game_start(cell_duration, false, new_state)
			
			screen_layer:removeEventListener(Event.MOUSE_DOWN, screen_touch)
		end
		screen_layer:addEventListener(Event.MOUSE_DOWN, screen_touch)
	end
	touch_to_start()
	---------------------------
end

-- Default function
function Play:onTransitionInBegin()
	print("Play - enter begin")
end

function Play:onTransitionInEnd()
	print("Play - enter end")
end

function Play:onTransitionOutBegin()
	print("Play - exit begin")
end

function Play:onTransitionOutEnd()
	print("Play - exit end")
end