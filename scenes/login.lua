-- This scene is to test all the new codes like: Facebook connect, database connect
-- require "ui"
Login = gideros.class(Sprite)

function Login:init(params)
	require "ui"
	crypto = Crypto.new();
	local left_margin = 30
	-- Name
	local alba_font = Font.new("fonts/ALBA____.txt", "fonts/ALBA____.png")
	
	local text = TextField.new(alba_font, "Name") -- Name label
	text:setPosition(left_margin, device_height / 5)
	text:setTextColor("0x000000")
	self:addChild(text)
	
	-- Password
	text = TextField.new(alba_font, "Password") -- Password label
	text:setPosition(left_margin, device_height / 5 + 30)
	text:setTextColor("0x000000")
	self:addChild(text)
	
	-- Error
	message = TextField.new(Font.new("fonts/ALBAS___.txt", "fonts/ALBAS___.png"), "") -- Password label
	message:setPosition((device_width - message:getWidth())/2, device_height / 5 + 60)
	self:addChild(message)

	local username = TextField2.new("Username")
	username:setPosition(left_margin + text:getWidth() + 20, device_height / 5 + 25)
	-- textfield2:setText("Hello giderans2!")
	username:setSize(150,25)
		
	local password = TextField2.new("Password")
	password:setPosition(left_margin + text:getWidth() + 20, device_height / 5 + 30 + 24)
	-- textfield4:setText("Hello giderans2!")
	password:setSize(150,25)
	
	-- Submit button
	local submit = Button.new()

	submit:setPosition((device_width - 130)/2, device_height / 5 + 120)
	submit:setSize(130, 30)

	submit:setTitle("Submit")
	submit:setTitleColor(255,255,255)
	submit:setBGColor(92/255,92/255,92/255)
	--submit:setFont("Verdana",12)
	
	
	addToRootView(username)
	addToRootView(password)
	addToRootView(submit)
	
	--[[
	local function input_entered(event)
		local t = event:getTarget()
		print(to_string(t))
		print "++_"
		print(to_string(event.text))
	end
	--]]
	
	local function button_submmitted(event)
		message:setText("Please wait")
		if username:getText() == "" or password:getText() == "" then
			message:setText("Please fill up the form")
			message:setTextColor("0xFF0000")
			message:setPosition((device_width - message:getWidth())/2, device_height / 5 + 60)
		else
			print(crypto:md5(password:getText()))
			
			-- Create new player
			local url = "https://api.scoreoid.com/v1/createPlayer"
			local postData = "api_key=cc7a88fd393a12c0ea2b5f369685905f354e79a4&game_id=d947ce5863&response=json&username=" .. username:getText() .. "&password=" .. crypto:md5(password:getText())
			local createPlayerRequest = UrlLoader.new(url, UrlLoader.POST, {}, postData)
			
			local function requestSuccess(event)
				if ( event.isError ) then
					print(event)
					print(event.data)
					print(to_string(event))
					
					message:setText("Please check your internet connection 1")
					message:setTextColor("0xFF0000")
					message:setPosition((device_width - message:getWidth())/2, device_height / 5 + 60)
				else
				---[[
					print(event)
					print("-----")
					print(event.data)
					print("-----")
					print(to_string(event))
					print("-----")
				--]]
					local response = JSON:decode(event.data)
					if response.success ~= nil then
						message:setText(response.success)
						message:setTextColor("0x13BF19")
					elseif response.error ~= nil then
						message:setText(response.error)
						message:setTextColor("0xFF0000")
					end
					message:setPosition((device_width - message:getWidth())/2, device_height / 5 + 60)
				end
			end
			
			local function networkError()
				message:setText("Please check your internet connection 2")
				message:setTextColor("0xFF0000")
				message:setPosition((device_width - message:getWidth())/2, device_height / 5 + 60)
			end
			
			createPlayerRequest:addEventListener(Event.COMPLETE, requestSuccess)
			createPlayerRequest:addEventListener(Event.ERROR, networkError)
		end
	end
	
	-- password:addEventListener("onTextFieldEdit", password_entered)
	submit:addEventListener("onButtonClick", button_submmitted)
end