End = gideros.class(Sprite)

function End:init(params)
	application:setBackgroundColor("0x8E8E88")

	local alba_font = Font.new("fonts/ALBAS___.txt", "fonts/ALBAS___.png")
	-- Your score
	local score_text = TextField.new(alba_font, "Your Score:")
	score_text:setPosition((device_width - score_text:getWidth())/2 - 10, device_height / 4)
	score_text:setTextColor("0xffffff")
	self:addChild(score_text)
	
	local score_real = TextField.new(alba_font, params.score)
	score_real:setPosition((device_width + score_text:getWidth())/2, device_height / 4)
	score_real:setTextColor("0xffffff")
	self:addChild(score_real)

	-- restart button
	local button_default = Bitmap.new(Texture.new("images/buttons/try_again.png"))
	local button_pushed = Bitmap.new(Texture.new("images/buttons/try_again.png"))
	 
	local restartButton = Button.new(button_default, button_pushed)

	restartButton:setPosition((device_width - restartButton:getWidth())/2, device_height / 4 + score_text:getHeight() + 20)
	self:addChild(restartButton)
	 
	restartButton:addEventListener("click", 
		function()  
			sceneManager:changeScene("play", 1, SceneManager.fade, easing.linear)
		end
	)
	
	-- share button
	local button_default = Bitmap.new(Texture.new("images/buttons/fbshare.png"))
	local button_pushed = Bitmap.new(Texture.new("images/buttons/fbshare.png"))
	 
	local shareButton = Button.new(button_default, button_pushed)

	shareButton:setPosition((device_width - shareButton:getWidth())/2, device_height / 4 + restartButton:getHeight() + 50)
	self:addChild(shareButton)
	
	shareButton:addEventListener("click", 
		function()  
			local params = {
				name = "Addictive puzzle games on mobile phone",
				caption = "Click the square as fast as possible",
				description = "I have just scored " .. params.score .. " at level " .. params.level .. ".",
				link = "https://developers.facebook.com/ios",
				picture = "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png"
			}
		 
			facebook:dialog("feed", params)
		end
	)
	
	-- Invite friend
	local button_default = Bitmap.new(Texture.new("images/buttons/recommend.png"))
	local button_pushed = Bitmap.new(Texture.new("images/buttons/recommend.png"))
	 
	local inviteButton = Button.new(button_default, button_pushed)

	inviteButton:setPosition((device_width - inviteButton:getWidth())/2, device_height / 4 + restartButton:getHeight() + shareButton:getHeight() + 60)
	self:addChild(inviteButton)
	
	inviteButton:addEventListener("click", 
		function()  
			local params = {
				message = "Check out this awesome app"
			}
			facebook:dialog("apprequests", params)
		end
	)
	
	-- Revive
	local button_default = Bitmap.new(Texture.new("images/buttons/revive.png"))
	local button_pushed = Bitmap.new(Texture.new("images/buttons/revive.png"))
	 
	local inviteButton = Button.new(button_default, button_pushed)

	inviteButton:setPosition((device_width - inviteButton:getWidth())/2, device_height / 4 + restartButton:getHeight() + shareButton:getHeight() + inviteButton:getHeight() + 80)
	self:addChild(inviteButton)
	
	print("Bubble type1: " .. params.bubble.type)
	inviteButton:addEventListener("click", 
		function()  
			sceneManager:changeScene("play", 1, SceneManager.fade, easing.linear, {userData = params})
		end
	)
end

-- Default function
function End:onTransitionInBegin()
	print("End - enter begin")
end

function End:onTransitionInEnd()
	print("End - enter end")
end

function End:onTransitionOutBegin()
	print("End - exit begin")
end

function End:onTransitionOutEnd()
	print("End - exit end")
end